import Component from "@ember/component";
import discourseComputed from "discourse-common/utils/decorators";
import { inject as service } from "@ember/service";
import { defaultHomepage } from "discourse/lib/utilities";
export default Component.extend({
  router: service(),

  @discourseComputed("router.currentRouteName", "router.currentURL")
  showHere(currentRouteName) {
    return currentRouteName === `discovery.${defaultHomepage()}`;
  }
});
