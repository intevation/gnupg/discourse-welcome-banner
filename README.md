# discourse-welcome-banner

A simple banner with an image for Discourse. Based on: [Discourse Welcome Link Banner](https://github.com/discourse/discourse-welcome-link-banner).

## Build tar.gz which you can install in Discourse

Run

`yarn build`

and you find a file named `discourse-welcome-banner.tar.gz` in the folder `dist`.
